# Node.js+Express+Typescript boilerplate

This is a boilerplate for typical application on Node/Express with TypeScript.

### Directories structure
- /config - configuration files
- /models - data layer contains any data sources awailable in the app (ORM files,
API libs, DB handlers)
- /routes - directory for typical Express route files 
- /src - any application logic lives here. If routes are controllers and models 
are data-access helpers then this folder stays for the application model. 
